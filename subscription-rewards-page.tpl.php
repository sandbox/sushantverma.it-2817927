<?php
 if(!empty($rondino_subscriber_rewards_switch) && isset($rondino_subscriber_rewards_switch)){
	if(isset($rdp_response_data["result_code"]) && ($rdp_response_data["result_code"] == "200")){
	  // "200": Success
	  $rdp_access_token = $rdp_response_data["access_token"];
	  $rdp_access_url = $rdp_response_data["access_url"];
	  echo '<div id="rondino_subscriber_rewards_page_iframe" style="height:1500px; width:100%; border:none;"><iframe style="height:100%;width:100%; border:none;" src="'.$rdp_access_url.'"></iframe></div>';
	}else{
	  //Print the message for the anonymous user.
	  print check_markup($rondino_subscriber_rewards_message_field['value'], $rondino_subscriber_rewards_message_field['format'], '',FALSE);			
	}
  }else{
	  if(isset($user_logged_status) && !empty($user_logged_status) && isset($access_user_path) && !empty($access_user_path) ){
			print 'Subscriber Rewards page is not currently active, <a href="/admin/config/services/subscriber_rewards" target="_blank">Click here</a> to configure it.';
		}else{			
			print 'Subscriber Rewards page is not currently active, please contact to the site administrator <a href="mailto:'.$site_email.'">'. $site_email .'</a>';
		}
	  }
?>
